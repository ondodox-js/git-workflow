## Getting and Creating Projects

## Name
`git init` Create an empty Git repository or reinitialize an existing one.

## Synopsis
`git init [-q | --quiet] [--bare] [--template=<template-directory>]
	  [--separate-git-dir <git-dir>] [--object-format=<format>]
	  [--ref-format=<format>]
	  [-b <branch-name> | --initial-branch=<branch-name>]
	  [--shared[=<permissions>]] [<directory>]`